import os
import sys
sys.path.append(os.path.realpath('.'))

import pymongo
from databases.mongodb import MongoDB

class PortaisNeMigration(object):

    _collection = MongoDB().getInstance().getCollection('portais_ne')

    def up(self):
        self._collection.create_index([('cpf_cnpj', pymongo.TEXT), ('codigo_elemento_despesa', pymongo.TEXT), ('descricao_natureza_despesa', pymongo.TEXT) , ('descricao_empenho', pymongo.TEXT)])
        self._collection.create_index([('valor_empenhado', pymongo.ASCENDING)], unique=False)
        self._collection.create_index([('tipo_natureza', pymongo.ASCENDING)], unique=False)
        self._collection.create_index([('data_emissao', pymongo.ASCENDING)], unique=False)
        

    def down(self):
        if self._collection.count() != 0:
            self._collection.drop()