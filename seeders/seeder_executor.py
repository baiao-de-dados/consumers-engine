import os
import sys
sys.path.append(os.path.realpath('.'))

from seeders.tces.ce import tces_seeder
from seeders.tces.ce import tce_ndo_seeder
from seeders.tces.ce import tces_municipios_seeder
from seeders.tces.ce import tces_municipios_orgaos_seeder
from seeders.tces.ce import tces_municipios_ne_seeder

def exec():
    tces_seeder.run()
    tce_ndo_seeder.run()
    tces_municipios_seeder.run()
    tces_municipios_orgaos_seeder.run()
    tces_municipios_ne_seeder.run()