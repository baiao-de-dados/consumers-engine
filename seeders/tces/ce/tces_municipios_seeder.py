import os
import sys
sys.path.append(os.path.realpath('.'))

from bson import ObjectId
from helpers.utils import Utils
from models.tce import Tce
from models.tce_municipio import TceMunicipio
from migrations.tces_municipios_migration import TcesMunicipiosMigration
from adapters.request_adapter import RequestAdapter

def run(exec_migration=True, save_json=False):

    ## Migration
    if exec_migration:
        tces_migration = TcesMunicipiosMigration()
        tces_migration.down()
        tces_migration.up()

    ## Seed
    tce = Tce()
    tce.load_one( { 'name': 'TCE_CE' } )
    tce_id = {'tce_id': ObjectId(tce._id) }

    municipios = {}
    try:
        municipios_res = RequestAdapter().getInstance().get(tce['api_url'] + tce['api_routes']['municipios'], timeout=20)
        municipios_res = municipios_res.json()
        municipios = municipios_res['rsp']['_content']

        if not municipios:
            raise Exception('Retorno vazio')

        if save_json:
            Utils.getInstance().createJsonData('municipios.json', municipios)
            
    except Exception as x:
        print('Error:', x.__class__.__name__)   

    tce_municipios = [dict(m, **tce_id) for m in municipios]
    for m in tce_municipios:
        tce_municipio = TceMunicipio( m )
        tce_municipio.save(is_many = True)