import os
import sys
sys.path.append(os.path.realpath('.'))

from bson import ObjectId
from helpers.utils import Utils
from models.tce import Tce
from models.tce_municipio import TceMunicipio
from models.tce_municipio_orgao import TceMunicipioOrgao
from migrations.tces_municipios_orgaos_migration import TcesMunicipiosOrgaosMigration
from adapters.request_adapter import RequestAdapter

def run(exec_migration=True, save_json=False):

    ## Migration
    if exec_migration:
        tces_m_orgaos_migration = TcesMunicipiosOrgaosMigration()
        tces_m_orgaos_migration.down()
        tces_m_orgaos_migration.up()

    # Seed
    tce = Tce()
    tce.load_one( { 'name': 'TCE_CE' } )
    tce_id = {'tce_id': ObjectId(tce._id) }

    tce_municipio = TceMunicipio()
    tce_municipio.load( tce_id )

    tce_municipios = tce_municipio['list']
    orgaos = {}
    for tce_m in tce_municipios:
        try:
            # TODO: Refatorar captura do ano/competencia
            orgaos_res = RequestAdapter().getInstance().get(tce['api_url'] + tce['api_routes']['orgaos'].format(tce_m['codigo_municipio'], "201900"), timeout=20)
            orgaos_res = orgaos_res.json()
            orgaos = orgaos_res['rsp']['_content']
            if not orgaos:
                raise Exception('Retorno vazio')
            
            if save_json:
                Utils.getInstance().createJsonData('orgaos_{}.json'.format(tce_m['codigo_municipio']), orgaos)
                
        except Exception as x:
            print('Error:', x.__class__.__name__)  

        municipio_id = {'municipio_id': ObjectId(tce_m['_id']) }
        tce_municipios_orgaos = [dict(m, **tce_id) for m in orgaos]
        tce_municipios_orgaos = [dict(m, **municipio_id) for m in tce_municipios_orgaos]
        for m_o in tce_municipios_orgaos:
            tce_m_orgao = TceMunicipioOrgao( m_o )
            tce_m_orgao.save(is_many = True)