import os
import sys
sys.path.append(os.path.realpath('.'))

from bson import ObjectId
from helpers.utils import Utils
from models.tce import Tce
from models.tce_municipio import TceMunicipio
from models.tce_municipio_orgao import TceMunicipioOrgao
from models.tce_municipio_ne import TceMunicipioNe
from migrations.tces_municipios_ne_migration import TcesMunicipiosNeMigration
from adapters.request_adapter import RequestAdapter

def run(exec_migration=True, save_json=False):

    ## Migration
    if exec_migration:
        tces_m_ne_migration = TcesMunicipiosNeMigration()
        tces_m_ne_migration.down()
        tces_m_ne_migration.up()

    # Seed
    tce = Tce()
    tce.load_one( { 'name': 'TCE_CE' } )
    tce_id = {'tce_id': ObjectId(tce._id) }

    tce_municipio = TceMunicipio()
    tce_municipio.load( tce_id )

    tce_municipios = tce_municipio['list']
    tce_municipio_orgaos = {}
    # print(tce_municipios)
    count = 0
    for tce_m in tce_municipios:
        municipio_id = {'municipio_id': ObjectId(tce_m['_id']) }
        # print(municipio_id)
        tce_municipio_orgao = TceMunicipioOrgao()
        tce_municipio_orgao.load( municipio_id )

        tce_municipio_orgaos = tce_municipio_orgao['list']
        # print(tce_municipio_orgaos)
        for m_o in tce_municipio_orgaos:
            meses = ['01', '02', '03', '04', '05', '06', '07', '08', '09' ,'10' ,'11', '12']
            for m in meses:
                try:
                    # TODO: Refatorar captura do ano/competencia
                    print(tce['api_url'] + tce['api_routes']['notas_empenhos'].format(tce_m['codigo_municipio'], m_o['codigo_orgao'], "2019{}".format(m) ))
                    m_ne_res = RequestAdapter().getInstance().get(tce['api_url'] + tce['api_routes']['notas_empenhos'].format(tce_m['codigo_municipio'], m_o['codigo_orgao'], "2019{}".format(m) ), timeout=20)
                    m_ne_res = m_ne_res.json()
                    m_ne = m_ne_res['rsp']['_content']

                    if not m_ne:
                        continue

                    # if save_json:
                        # Utils.getInstance().createJsonData('orgaos_{}.json'.format(tce_m['codigo_municipio']), orgaos)
                except Exception as x:
                    print('Error:', x.__class__.__name__)  

                orgao_id = {'orgao_id': ObjectId(m_o['_id']) }
                tce_municipio_ne = [dict(m, **tce_id) for m in m_ne]
                tce_municipio_ne = [dict(m, **municipio_id) for m in tce_municipio_ne]
                tce_municipio_ne = [dict(m, **orgao_id) for m in tce_municipio_ne]
                for mne in tce_municipio_ne:
                    tce_m_ne = TceMunicipioNe( mne )
                    tce_m_ne.save(is_many = True)