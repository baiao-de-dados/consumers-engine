import os
import sys
sys.path.append(os.path.realpath('.'))

from helpers.utils import Utils
from models.tce import Tce
from migrations.tces_migration import TcesMigration

def run(exec_migration=True, save_json=False):

    ## Migration
    if exec_migration:
        tces_migration = TcesMigration()
        tces_migration.down()
        tces_migration.up()

    # Seed
    tce_ce =  Utils.getInstance().getTCEObject('ce')
    tce = Tce( tce_ce )
    tce.save()