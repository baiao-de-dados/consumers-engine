import os
import sys
sys.path.append(os.path.realpath('.'))

import pandas as pd
import itertools
from io import StringIO
from bson import ObjectId
from helpers.utils import Utils
from models.tce import Tce
from models.tce_ndo import TceNdo
from migrations.tces_ndo_migration import TcesNdoMigration 

def run(exec_migration=True, save_json=False):

    ## Migration
    if exec_migration:
        tces_migration = TcesNdoMigration()
        tces_migration.down()
        tces_migration.up()

    ## Seed
    tce = Tce()
    tce.load_one( { 'name': 'TCE_CE' } )
    exercicio = 2019

    n_sheets = 4
    s_dicts = {}
    ndo_file = Utils.getInstance().getTCENDOFile('natureza_da_despesa_orcamentaria', str(exercicio), 'ce')

    if ndo_file != None:
        counter = 0
        while(counter < n_sheets):
            df = pd.read_excel(ndo_file, sheet_name=counter)
            df = pd.read_csv( StringIO( df.to_csv() ), index_col=0 )
            
            s_dicts['s{}'.format(counter)] = dict(df.iloc[2:].values)    
            counter += 1

        seperator_cd = '.'
        seperator_desc = '__'
        for s in itertools.product(s_dicts['s0'].keys(), s_dicts['s1'].keys(), s_dicts['s2'].keys(), s_dicts['s3'].keys() ):
            cd_despesa = seperator_cd.join(s)
            desc_despesa = [Utils.getInstance().removeSpecialCharacter(s_dicts['s{}'.format(i)].get(s[i]) ) for i in range(len(s))]
            desc_despesa = seperator_desc.join(desc_despesa)
            tce_ndo = TceNdo({'cd_despesa': cd_despesa, 'desc_despesa': desc_despesa,'exercicio': exercicio, 'tce_id': ObjectId(tce._id)})
            tce_ndo.save(is_many=True)