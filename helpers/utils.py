from pathlib import Path
import json
import io
import yaml
import unicodedata
import re
from difflib import SequenceMatcher

class Utils(object):
    _TCE_FILE_OBJECT='tce.yml'
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(Utils, cls).__new__(cls)

        return cls._instance    

    @staticmethod 
    def getInstance():
        if Utils._instance == None:
            Utils()
        return Utils._instance

    def getRootDir(self) -> Path:
        return Path(__file__).parent.parent

    def getTCEDataDir(self, state = 'ce') -> Path:
        return self.getRootDir().joinpath('data').joinpath('tces').joinpath(state) 
    
    def getConsumerDataDir(self) -> Path:
        return self.getRootDir().joinpath('consumers')
    
    def getTCEObject(self, state = 'ce'):
        tce_yml = self.getTCEDataDir(state).joinpath(self._TCE_FILE_OBJECT)

        with open(tce_yml, 'r') as stream:
            tce = yaml.safe_load(stream)

        return tce

    def getConsumerObject(self, consumer_type = 'web-crawler', group = 'aspec' ):
        consumer_yml = self.getConsumerDataDir().joinpath(consumer_type).joinpath(group).joinpath("{}.yml".format(group))

        with open(consumer_yml, 'r') as stream:
            consumer = yaml.safe_load(stream)

        return consumer

    def createJsonData(self, json_filename, obj, tce = 'ce'):
        try:
            json_dir = self.getTCEDataDir(tce).joinpath('json_data').joinpath(json_filename)
            with open(json_dir, 'w') as fp:
                json.dump(obj, fp)

        except Exception as x:
            print('Error:', x.__class__.__name__)  

    def getTCENDOFile(self, filename, year, state = 'ce'):
        return self.getTCEDataDir(state).joinpath('ndo_sheets').joinpath(year).joinpath('{}.xlsx'.format(filename) )

    def removeSpecialCharacter(self, s):
        nfkd = unicodedata.normalize('NFKD', s)
        s = u"".join([c for c in nfkd if not unicodedata.combining(c)])
        return re.sub('[^a-zA-Z0-9 \\\]', '', s)
    
    def mesStringToNum(self, mes_str):
        m = {
            'janeiro': '01', 'fevereiro': '02', 'marco': '03', 'abril':'04',
            'maio':'05', 'junho':'06', 'julho':'07', 'agosto':'08',
            'setembro':'09', 'outubro':'10', 'novembro':'11', 'dezembro':'12'
        }
        s = mes_str.strip().lower()
        return m[s]

    def scoreStringMacther(self, str1, str2):
        s = SequenceMatcher(None, str1, str2)
        return s.ratio()
