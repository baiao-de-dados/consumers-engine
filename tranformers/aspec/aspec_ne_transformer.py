import os
import sys
sys.path.append(os.path.realpath('.'))

from helpers.utils import Utils
from difflib import SequenceMatcher
import re
import locale
from datetime import datetime

class AspecNeTransformer(object):
    
    _instance = None

    ne_to_doc = {
        'empenho': 'num_empenho',
        'data': 'data_emissao',
        'tipo': 'tipo',
        'credor': 'credor',
        'cnpj': 'cpf_cnpj',
        'modalidade': 'modalidade',
        'numero_da_licitacao': 'numero_da_licitacao',
        'unidade_orcamentaria': 'unidade_orcamentaria',
        'funcao': 'funcao',
        'subfuncao': 'subfuncao',
        'programa_de_governo': 'programa_governo',
        'projeto_atividade': 'projeto_atividade',
        'natureza_da_despesa': 'natureza_despesa',
        'fonte_de_recurso': 'fonte_recurso',
        'historico': 'descricao_empenho',
        'fases_despesa': 'fases_despesa'
    }
    
    fne_to_doc = {
        'documento': 'documento',
        'data': 'data',
        'tipo': 'tipo',
        'registro': 'registro',
        'valor_r': 'valor'
    }

    # e_fields = [
    #     "n_empenho",
    #     "credor",
    #     "cpf_cnpj",
    #     "unidade_orcamentaria",
    #     "codigo_elemento_despesa",
    #     "descricao_natureza_despesa", # regra a partir da natureza de despesa
    #     "funcao",
    #     "subfuncao",
    #     "programa_governo",
    #     "descricao_empenho",
    #     "fonte_recurso",
    #     "fases_despesa",
    #     "valor_empenho", # regra a apartir das fases de des pesas
    #     "data_emissao", # extracao de data a partir do campo data do empenho
    #     "tipo_natureza" # despesa default
    # ]

    # f_fields = [
    #     "documento",
    #     "data", # usar converter para data
    #     "tipo", 
    #     "registro",
    #     "valor"
    # ]

    def __new__(cls, fields = None):
        if cls._instance is None:
            cls._instance = super(AspecNeTransformer, cls).__new__(cls)

        return cls._instance

    @staticmethod 
    def getInstance():
        if AspecNeTransformer._instance == None:
            AspecNeTransformer()
        return AspecNeTransformer._instance

    def gen(self, ne, ignore_fields=False):

        if ignore_fields:
            return ne

        for k_ne in list(ne):

            v_ne = ne.pop(k_ne)
            s_key_ne = re.sub(' +', ' ', Utils.getInstance().removeSpecialCharacter(k_ne).lower() ).replace(' ', '_')
            
            new_key_ne = s_key_ne
            if s_key_ne in self.ne_to_doc:
                new_key_ne = self.ne_to_doc[s_key_ne]

            ne[new_key_ne] = v_ne

            # TODO: Refactor
            if new_key_ne == 'fases_despesa':
                f_ne = []
                for dic_f_ne in ne[new_key_ne]:
                    for k_f_ne in list(dic_f_ne):
                        v_f_ne = dic_f_ne.pop(k_f_ne)
                        s_key_f_ne = re.sub(' +', ' ', Utils.getInstance().removeSpecialCharacter(k_f_ne).lower() ).replace(' ', '_')

                        new_key_f_ne = s_key_f_ne
                        if s_key_f_ne in self.fne_to_doc:
                            new_key_f_ne = self.fne_to_doc[s_key_f_ne]

                        dic_f_ne[new_key_f_ne] = v_f_ne

                    f_ne.append(dic_f_ne)
                ne[new_key_ne] = f_ne
        
        return self.posGen(ne)
    

    def posGen(self, ne):
        
        new_ne = dict(ne)
        # 1
        nat_des = new_ne['natureza_despesa'].split('-')
        # codigo_elemento_despesa
        new_ne.update({'codigo_elemento_despesa' : nat_des[0].strip() })
        # descricao_natureza_despesa
        new_ne.update({'descricao_natureza_despesa' : nat_des[1].strip() })

        del new_ne['natureza_despesa']

        # 2
        v_empenho = '0'
        d_empenho = ''
        if new_ne['fases_despesa'] != []:

            # f_ne = []
            fases_ne = new_ne['fases_despesa']
            #
            locale.setlocale(locale.LC_TIME, 'pt_BR.utf8')
            for dic_f_ne in fases_ne:
                dt = datetime.strptime(dic_f_ne['data'], '%d de %B de %Y') #.strftime('%d/%m/%Y')
                # print(dt)
                dic_f_ne['data'] = datetime(dt.year, dt.month, dt.day, 0 , 0)

            if fases_ne[0]['tipo'] == 'Empenho':
                v_empenho = fases_ne[0]['valor']
                d_empenho = fases_ne[0]['data']
                # num_empenho
                new_ne.update({ 'num_empenho' : fases_ne[0]['documento']})
            

        # valor_empenho
        new_ne.update({ 'valor_empenho' : v_empenho})
        # data_emissao
        new_ne.update({ 'data_emissao' : d_empenho})

        return new_ne