import os
import sys
sys.path.append(os.path.realpath('.'))


from adapters.request_adapter import RequestAdapter
from helpers.utils import Utils
from helpers.html_parser import HTMLParser
import requests
from bs4 import BeautifulSoup  # https://pypi.org/project/beautifulsoup4/
import pandas as pd
import re
import datetime
import urllib.parse as ulp
from pygrok import Grok

from difflib import SequenceMatcher
# import jellyfish
# from fuzzywuzzy import fuzz
from tranformers.aspec.aspec_ne_transformer import AspecNeTransformer
from migrations.portais_ne_migration import PortaisNeMigration
from models.portal_ne import PortalNe


def run(exec_migration=True, save_json=False):

    ## Migration
    if exec_migration:
        portais_ne_migration = PortaisNeMigration()
        portais_ne_migration.down()
        portais_ne_migration.up()
    # TODO: Passar para despesas_mapper.py
    page = requests.get(
        'https://www.governotransparente.com.br/transparencia/1421489/detalharempenhoportal/7/598719?clean=false'
    )
    soup = BeautifulSoup(page.text, 'html.parser')
    # Pegar todo o texto da div BodyText
    info_names = soup.find(class_='col-xs-12')
    # exibir detalhes do empenho
    list_items = info_names.find_all('li')
    keys = ['Empenho', 'Data']
    for item_name in list_items[1:]:
        item_name.string.find(':')
        string = item_name.string
        lim = string.find(':')
        key = string[0:lim]
        texto = string[lim + 1:]
        texto = " ".join(texto.split())
        if lim == -1:
            keys.append('Modalidade')
        else:
            keys.append(key)

    # -------------------------------- script principal --------------------------------# 
    cd_municipio = 1421489
    url_portal = 'https://www.governotransparente.com.br/transparencia/{}/'.format(cd_municipio)

    aspec_consumer = Utils.getInstance().getConsumerObject(group='aspec')
    ano_consumer = 7
    data_ini = ulp.quote('01/01/2019', safe='')
    data_fim = ulp.quote('31/12/2019', safe='')
    date_consumer_pattern = '%{MONTHDAY:day}/%{MONTHNUM:month}/%{YEAR:year}'
    grok = Grok(date_consumer_pattern)


    url_consumer = url_portal + aspec_consumer['webc_routes']['get_empenhos'].format(ano_consumer, data_ini, data_fim)

    res_text =  RequestAdapter().getInstance().get(url_consumer, timeout = 30).text
    soup = BeautifulSoup(res_text, 'html.parser')
    lista_empenhos = re.findall("detalharempenhoportal/{}/(.*)?clean".format(ano_consumer), str(soup.html) )
    lista_empenhos = [empenho.replace('?', '') for empenho in lista_empenhos]
    df_empenhos = pd.DataFrame(columns=keys)

    ne = {}
    ne_fases = {}

    # for num_emp, empenho in enumerate(lista_empenhos[:1]):
    for num_emp, empenho in enumerate(lista_empenhos):

        url = url_portal + aspec_consumer['webc_routes']['get_empenho'].format(ano_consumer, empenho)
        page = RequestAdapter().getInstance().get(url, timeout=30)
        soup = BeautifulSoup(page.text, 'html.parser')

        info_names = soup.find(class_='col-xs-12')
        list_items = info_names.find_all('li')

        dict_temp = {}
        dict_temp['Empenho'] = empenho
        data_emissao = grok.match(list_items[0].string)
        dict_temp['Data'] = data_emissao['day']+'/'+data_emissao['month']+'/'+data_emissao['year']
        for num, item_name in enumerate(list_items[1:]):
            item_name.string.find(':')
            string = item_name.string

            lim = string.find(':')

            key = string[0:lim]
            texto = string[lim + 1:]
            texto = " ".join(texto.split())

            if lim == -1:
                key = 'Modalidade'

            dict_temp[key] = texto
        # exibir movimentos do empenho
        movimentos = info_names.find_all('tr')
        keys_movimentos = [i.string for i in movimentos[0].find_all('th')]
        df_movimentos = pd.DataFrame(columns=keys_movimentos)

        for movimento in movimentos[1:]:
            dict_temp_mov = {}
            for num, i in enumerate(movimento.find_all('td')):
                texto = i.string
                dict_temp_mov[keys_movimentos[num]] = texto
            df_movimentos = df_movimentos.append(dict_temp_mov, ignore_index=True)

        ne_fases =  df_movimentos.to_dict(orient='records')
        df_empenhos = df_empenhos.append(dict_temp, ignore_index=True)

        ne = dict(df_empenhos.to_dict(orient='records')[0])
        # tipo_natureza
        ne.update({ 'tipo natureza' : 'despesa' })
        # fases_despesa
        ne.update({ 'fases despesa' : ne_fases })
        # data_atualizacao
        ne.update({ 'data atualizacao' : datetime.datetime.now() })
        # url_despesa
        ne.update({ 'url despesa' : url })
        # cd_municipio
        ne.update({ 'cd municipio' : cd_municipio })

        #########
        # print( AspecNeTransformer().getInstance().gen(ne) )
        portal_ne = PortalNe( AspecNeTransformer().getInstance().gen(ne) )
        portal_ne.save(is_many=True)



# AspecNeTransformer().getInstance().gen(ne)
# run()