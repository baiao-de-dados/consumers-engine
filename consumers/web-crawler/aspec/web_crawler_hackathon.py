import requests
from bs4 import BeautifulSoup  # https://pypi.org/project/beautifulsoup4/
import pandas as pd
import re

# -------------------------------- primeiro bloco de código para encontrar as 'colunas',  -------------------------------- 
# --------------------------------                 pode ser melhorado dps                 -------------------------------- 
page = requests.get(
    'https://www.governotransparente.com.br/transparencia/1421489/detalharempenhoportal/7/598719?clean=false'
)
soup = BeautifulSoup(page.text, 'html.parser')
# Pegar todo o texto da div BodyText
info_names = soup.find(class_='col-xs-12')
# exibir detalhes do empenho
list_items = info_names.find_all('li')
keys = ['Empenho', 'Data']
for item_name in list_items[1:]:
    item_name.string.find(':')
    string = item_name.string
    lim = string.find(':')
    key = string[0:lim]
    texto = string[lim + 1:]
    texto = " ".join(texto.split())
    if lim == -1:
        keys.append('Modalidade')
    else:
        keys.append(key)



# -------------------------------- script principal -------------------------------- 

pesquisa = 'https://www.governotransparente.com.br/transparencia/1421489/consultarempenho/resultado?ano=7&inicio=01%2F01%2F2019&fim=31%2F12%2F2019&unid=-1&valormax=&valormin=&credor=-1&clean=false&datainfo=35Vjg%2BlEOzUC6mPPugOiLUbuCOgjdEdeT8etwENYYCI%3D'

soup = BeautifulSoup(requests.get(pesquisa).text, 'html.parser')
lista_empenhos = re.findall("detalharempenhoportal/7/(.*)?clean",
                            str(soup.html))

lista_empenhos = [empenho.replace('?', '') for empenho in lista_empenhos]

df_empenhos = pd.DataFrame(columns=keys)

for num_emp, empenho in enumerate(lista_empenhos[0:1]):
    url = 'https://www.governotransparente.com.br/transparencia/1421489/detalharempenhoportal/7/{}?clean=false'.format(
        empenho)
    page = requests.get(url)
    soup = BeautifulSoup(page.text, 'html.parser')
    info_names = soup.find(class_='col-xs-12')
    list_items = info_names.find_all('li')

    dict_temp = {}
    dict_temp['Empenho'] = empenho
    dict_temp['Data'] = list_items[0].string
    for num, item_name in enumerate(list_items[1:]):
        item_name.string.find(':')
        string = item_name.string

        lim = string.find(':')

        key = string[0:lim]
        texto = string[lim + 1:]
        texto = " ".join(texto.split())

        if lim == -1:
            key = 'Modalidade'

        dict_temp[key] = texto

        # exibir movimentos do empenho
        movimentos = info_names.find_all('tr')
        keys_movimentos = [i.string for i in movimentos[0].find_all('th')]
        df_movimentos = pd.DataFrame(columns=keys_movimentos)

        for movimento in movimentos[1:]:
            dict_temp_mov = {}
            for num, i in enumerate(movimento.find_all('td')):
                texto = i.string
                dict_temp_mov[keys_movimentos[num]] = texto
            df_movimentos = df_movimentos.append(dict_temp_mov,
                                                 ignore_index=True)
            df_movimentos['Empenho'] = empenho

        # print(df_movimentos.head())
        df_movimentos.to_csv('{:05d}_movimentacao_empenho_{}.csv'.format(
            num_emp, empenho),
                             index=False,
                             sep=',',
                             encoding='UTF-8')

    df_empenhos = df_empenhos.append(dict_temp, ignore_index=True)

# print(df_empenhos.head())

df_empenhos.to_csv('sao_goncalo_amarante_2019.csv',
                   index=False,
                   sep=',',
                   encoding='UTF-8')
