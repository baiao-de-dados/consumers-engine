import os
import sys
sys.path.append(os.path.realpath('.'))


from adapters.request_adapter import RequestAdapter
from helpers.utils import Utils
import requests
from bs4 import BeautifulSoup  # https://pypi.org/project/beautifulsoup4/
import pandas as pd
import re
import urllib.parse as ub


print('QUOTE: ', ub.quote('01/01/2019', safe=''))
print('UNQUOTE: ', ub.unquote('01%2F01%2F2019'))