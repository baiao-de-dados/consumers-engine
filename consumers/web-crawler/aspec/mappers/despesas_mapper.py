import os
import sys
sys.path.append(os.path.realpath('.'))


from adapters.request_adapter import RequestAdapter
import requests
from bs4 import BeautifulSoup  # https://pypi.org/project/beautifulsoup4/
import pandas as pd
import re

# -------------------------------- primeiro bloco de código para encontrar as 'colunas',  -------------------------------- 
# --------------------------------                 pode ser melhorado dps                 -------------------------------- 
page = requests.get(
    'https://www.governotransparente.com.br/transparencia/1421489/detalharempenhoportal/7/598719?clean=false'
)
soup = BeautifulSoup(page.text, 'html.parser')
# Pegar todo o texto da div BodyText
info_names = soup.find(class_='col-xs-12')
# exibir detalhes do empenho
list_items = info_names.find_all('li')
keys = ['Empenho', 'Data']
for item_name in list_items[1:]:
    item_name.string.find(':')
    string = item_name.string
    lim = string.find(':')
    key = string[0:lim]
    texto = string[lim + 1:]
    texto = " ".join(texto.split())
    if lim == -1:
        keys.append('Modalidade')
    else:
        keys.append(key)

# Mapear exercicio no site aspec

# mapear estrutura empenho (bind)