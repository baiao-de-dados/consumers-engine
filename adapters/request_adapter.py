import grequests
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

class RequestAdapter(object):
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(RequestAdapter, cls).__new__(cls)
        return cls._instance    

    @staticmethod 
    def getInstance():
        if RequestAdapter._instance == None:
            RequestAdapter()
        return RequestAdapter._instance
    
    def get(self, *args, **kwargs):
        # TODO: Headers Credentials
        # s = grequests.Session()
        # s.headers.update({'x-test': 'true'})
        return self.req_retry_session(session=None).get(*args, **kwargs)

    def req_retry_session(self, retries=3, backoff_factor=0.3, status_forcelist=(500, 502, 504), session=None):
        session = session or grequests.Session()
        retry = Retry( total=retries, read=retries, connect=retries, backoff_factor=backoff_factor, status_forcelist=status_forcelist, )

        adapter = HTTPAdapter(max_retries=retry)
        session.mount('http://', adapter)
        session.mount('https://', adapter)
        
        return session