from pymongo import MongoClient

class MongoDB(object):

    _instance = None

    def __new__(cls, dbname = None):
        if cls._instance is None:
            cls._instance = super(MongoDB, cls).__new__(cls)
            # TODO: Pegar valores do arquivos de configuracao
            # self._conn = MongoClient( 'mongodb://{}:{}@{}:{}'.format('root', 'root', 'localhost', 27017) )
            cls._conn = MongoClient( 'mongodb://{}:{}'.format('localhost', 27017) )
        cls._db   = cls._conn[ dbname or 'bdhackathon' ]

        return cls._instance    

    @staticmethod 
    def getInstance():
        if MongoDB._instance == None:
            MongoDB()
        return MongoDB._instance
    
    def getDb(self):
        return self._db

    def getCollection(self, name=''):
        return self._db[name]