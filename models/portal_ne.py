import os
import sys
sys.path.append(os.path.realpath('.'))

from .base_model import BaseModel
from databases.mongodb import MongoDB

class PortalNe(BaseModel):
    
    _collection = MongoDB().getInstance().getCollection('portais_ne')

    @property
    def keywords(self):
        return self.name.split()