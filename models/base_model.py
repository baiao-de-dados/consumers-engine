import os
import sys
sys.path.append(os.path.realpath('.'))

from bson import ObjectId
from collections import ChainMap
from databases.mongodb import MongoDB

class BaseModel(dict):
    __getattr__ = dict.get
    __delattr__ = dict.__delitem__
    __setattr__ = dict.__setitem__

    def save(self, is_many = False):
        if not self._id:
            self._collection.insert(self)
            if is_many:
                self.clear()
        else:
            self._collection.update(
                { "_id": ObjectId(self._id) }, self)

    def load_by_objID(self, objId):
        self.clear()
        self.update(self._collection.find_one({"_id": ObjectId(objId)}))
    
    def load_one(self, obj = {}):
        self.clear()
        self.update(self._collection.find_one(obj))

    def load(self, obj = {}):
        self.clear()
        obj_list = {}
        obj_list['list'] = list( self._collection.find(obj) )

        self.update( obj_list )

    def remove(self):
        if self._id:
            self._collection.remove({"_id": ObjectId(self._id)})
            self.clear()